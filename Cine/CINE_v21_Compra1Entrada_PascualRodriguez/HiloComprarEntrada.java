package CINE_v21_Compra1Entrada_PascualRodriguez;

class HiloComprarEntrada implements Runnable {

    Cine cine;

    public HiloComprarEntrada(Cine cine) {
        this.cine = cine;
    }

    @Override
    public void run() {

        try {
            cine.compraEntradaPelicula();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Cine getCine() {
        return cine;
    }

    public void setCine(Cine cine) {
        this.cine = cine;
    }

}