package psp_semi_actividades.Tema1;

import java.util.Map;

/**
 * Task1
 */
public class Task1 {

    public static void main(String[] args) {
        ProcessBuilder processBuilder = new ProcessBuilder();        
        Map<String, String> environment = processBuilder.environment();
        environment.forEach((key, value) -> System.out.println("Variable: "+key+"\t" +"Valor: " +value));
    }
}