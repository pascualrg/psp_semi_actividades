package psp_semi_actividades.Tema1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Task6 {
    public static void main(String[] args) throws IOException {
        
        String linea = " ";

            System.out.println("\nEste programa solo acepta los comandos \"ls\", \"grep\", \"nano\", \"cat\"");
            System.out.println("Para finalizar el programa, deje la linea vacía.");
            System.out.println("-----------------------------------------------------------------------------------------");

            while (linea.length()>0){

                System.out.print("\nminishell> ");
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
                linea = br.readLine();

                System.out.println("\n");
                if (linea.length()>0){

                    String comando = "";
                    //Si el comando tiene mas de un espacio
                    if(linea.split(" ").length>1){
                        comando = linea.substring(0, linea.indexOf(" "));
                    }else{//si el comando no tiene espacios, pasará el comando en si, ej: ls
                        comando = linea;
                    }

                    if(comando.equals("ls") || comando.equals("grep") || comando.equals("cat") || comando.equals("nano")){
                        
                        //creará un array de Strings por cada espacio
                        //del comando introducido por el usuario.
                        String [] comandos = linea.split(" ");

                        
                        ProcessBuilder pb = new ProcessBuilder(comandos);
                        System.out.print(">Ejecutando el comando: ");
                        for (int i = 0; i < comandos.length; i++) {
                            System.out.print(comandos[i]+" ");
                        }
                        System.out.println("");
                        pb.inheritIO();
                        pb.start();
                        System.out.println("\n");                       
        
                    }else{
                        System.out.println("Comando desconocido.");
                    }
                    
                }

            }
            
            

        

    }
    
}