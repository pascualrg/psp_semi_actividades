package psp_semi_actividades.Tema1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Task4 {

    public static void main(String[] args) throws IOException {
        
        System.out.println("Programa que calcula el valor HASH MD5 dado una linea de texto.");
        System.out.println("Acaba con una linea vacía.");
        System.out.println("-------------------------------------------------------------------------");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //"lector"
        String linea = " ";

      
        System.out.print("md5txt> ");
        linea = br.readLine(); //Se lee el texto a cifrar con readLine() que retorna un String con el texto
        while (linea.length()>0){

            System.out.print("Codificacion: ");
            ProcessBuilder pb = new ProcessBuilder("echo", "-n", linea);
            ProcessBuilder pb1 = new ProcessBuilder("md5sum");

            pb1.redirectOutput(ProcessBuilder.Redirect.INHERIT);

            List<ProcessBuilder> builders = new ArrayList<ProcessBuilder>();

            builders.add(pb);
            builders.add(pb1);

            List<Process> procesos = ProcessBuilder.startPipeline(builders);

            linea = br.readLine(); //Se lee el texto a cifrar con readLine() que retorna un String con el texto


        }

        System.out.println("\nFinalizacion del programa.");
    }
    
}

