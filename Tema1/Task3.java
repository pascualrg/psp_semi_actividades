package psp_semi_actividades.Tema1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;


public class Task3 {
    
    public static void contarPalabras(Process p, String palabras){
        OutputStream os = p.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        try {
            bw.write(palabras);
            bw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    public static void resultado(Process p){

        InputStream is = p.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);

        String l = null;

        System.out.println("\nPalabras    Letras");
        System.out.println("========    =======");
        try {
            while((l=br.readLine())!=null){

                System.out.println(l);

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {

        ProcessBuilder pb = new ProcessBuilder("wc", "-m", "-w");
        System.out.print("\ncontadortxt> ");
        Scanner en = new Scanner(System.in);
        String entradaUser = en.nextLine();

        while(entradaUser.length()!=0){

            try {
                
                Process p = pb.start();
                contarPalabras(p, entradaUser);
                p.waitFor();
                resultado(p);


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.print("\ncontadortxt> ");
            entradaUser = en.nextLine();

        }

        en.close();

        System.out.println("\nFinalizacion del programa.");


    }
    
}
