package psp_semi_actividades.Tema1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;


public class Task5 {

    public static void bienvenida(){
        System.out.println("");
        System.out.println("2n programa que calcula el valor HASH MD5 dado una linea de texto.");
        System.out.println("Acaba con una linea vacía.");
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("");
    }
    
    public static void codificar(Process p, String textoUser){
        OutputStream os = p.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        try {
            bw.write(textoUser);
            bw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    public static void resultado(Process p){

        InputStream is = p.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);

        String l = null;

        System.out.print("\nCodificación MD5: ");
        try {
            while((l=br.readLine())!=null){

                System.out.println(l);

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {

        
        bienvenida();
        System.out.print("\nmd5txt> ");
        Scanner en = new Scanner(System.in);
        String entradaUser = en.nextLine();

        ProcessBuilder pb = new ProcessBuilder("md5sum");

        while(entradaUser.length()!=0){

            try {
                
                Process p = pb.start();
                codificar(p, entradaUser);
                p.waitFor();
                resultado(p);


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.print("\nmd5txt> ");
            entradaUser = en.nextLine();

        }

        en.close();

        System.out.println("\nFinalizacion del programa.");


    }
    
}
