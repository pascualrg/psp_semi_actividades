package psp_semi_actividades.Tema2.Task2_1_Ford;

public class LiniaMuntatge implements Runnable {
    private String nombre;
    private Component[] component = new Component[3];

    int compEnsamblados;

    public LiniaMuntatge(Component c1, Component c2, Component c3, String nombreLinea) {
        nombre = nombreLinea;
        System.out.println("[" + Thread.currentThread().getName() + "] Iniciando la linia de montaje " + nombre);
        this.component[0] = c1;
        this.component[1] = c2;
        this.component[2] = c3;
        compEnsamblados = 0;
    }

    public void buscar(Component c){
        System.out.println("[" + Thread.currentThread().getName() + "] Buscando el componente " + c.getNomComponent());
        recoger(c);
    }

    public void recoger(Component c){
        if(!c.isRecogido()){
            System.out.println("[" + Thread.currentThread().getName() + "] Recoge el componente " + c.getNomComponent());
            c.setRecogido(true);
        }else{
            System.out.println("[" + Thread.currentThread().getName() + "] Esperando nuevo componente");
        }
        ensamblar(c);
    }

    public void ensamblar(Component c){
        System.out.println("[" + Thread.currentThread().getName() + "] Ensambla el componente " + c.getNomComponent());
        compEnsamblados++;

        if(compEnsamblados==3){
            System.out.println("[" + Thread.currentThread().getName() + "] COCHE MONTADO en la "+ Thread.currentThread().getName());
        }

    }

    public void run() {
       
        for (int i = 0; i < component.length; i++) {
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buscar(component[i]);
        }  

    }
}
