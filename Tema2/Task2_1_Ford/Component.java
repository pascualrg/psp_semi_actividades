package psp_semi_actividades.Tema2.Task2_1_Ford;
import java.io.*;

public class Component{
	//attr
	private String nombreComponente;
	private boolean disponible;
    private boolean recogido;

    public Component( String nom) throws IOException {
		try {
			this.setNomComponent(nom);
			System.out.println("["+Thread.currentThread().getName()+"] Encargando  1 componente "+ getNomComponent()+" al dept VENTAS...");
			Thread.sleep(30);
			System.out.println("["+Thread.currentThread().getName()+"] Nuevo componente " + getNomComponent() + " disponible.");
			disponible = true;
            recogido = false;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String getNomComponent() {
		return nombreComponente;
	}

	public void setNomComponent(String nomComponent) {
		this.nombreComponente = nomComponent;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		try {
			Thread.sleep(1);
			this.disponible = disponible;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

    public boolean isRecogido(){
        return recogido;
    }

    public void setRecogido(boolean ensamblado) {
		try {
			Thread.sleep(1);
			this.recogido = ensamblado;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
