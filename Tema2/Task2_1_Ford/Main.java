package psp_semi_actividades.Tema2.Task2_1_Ford;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {

		System.out.println("Programa de montaje de COCHES FORD");
		System.out.println("=====================================");

		Component carroceria = new Component("CARROCERIA");
		Component motor = new Component("MOTOR");
		Component ruedas = new Component("RUEDAS");
		
		LiniaMuntatge l1 = new LiniaMuntatge(carroceria, motor, ruedas, "lineaA");
		LiniaMuntatge l2 = new LiniaMuntatge(carroceria, motor, ruedas, "lineaB");
		LiniaMuntatge l3 = new LiniaMuntatge(carroceria, motor, ruedas, "lineaC");

		Thread hilo1 = new Thread(l1, "liniaA");
		Thread hilo2 = new Thread(l2, "liniaB");
		Thread hilo3 = new Thread(l3, "liniaC");

		hilo1.start();
		hilo2.start();
		hilo3.start();
		
		hilo1.join();
		hilo2.join();
		hilo3.join();
				
	}

}
