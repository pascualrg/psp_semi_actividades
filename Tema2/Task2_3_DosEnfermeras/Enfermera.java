package psp_semi_actividades.Tema2.Task2_3_DosEnfermeras;

public class Enfermera {

    String nombre;
    private int maxPacientes;
    int numPacientesAsignados = 0;

    public Enfermera (int maxPacientes){
        this.maxPacientes = maxPacientes;
    }

    public void setNombreEnfermera(String nombre){
        this.nombre = nombre;
    }

    public String getNombreEnfermera(){
        return nombre;
    }

    CuadroCitas cCitas = new CuadroCitas();

    public boolean asignarCita(String paciente){

        numPacientesAsignados++;
        String[] horas = {"10:00h", "10:15h", "10:30h", "10:45h", "11:00h", "11:15h", "11:30h", "11:45h"};

        boolean quedanCitas = false;

        if(numPacientesAsignados<=maxPacientes){
            cCitas.lineaCuadro.add(horas[numPacientesAsignados-1]+" --> "+paciente);
            System.out.println("["+paciente+"] "+this.nombre+" asgina la Cita Previa al paciente "+paciente+" a las "+horas[numPacientesAsignados-1]);
        }else{
            System.out.println("["+paciente+"] "+this.nombre+" Enfermera indica que no quedan turnos, Cita Previa imposible");
        }

        return quedanCitas;

    }

    public void mostrarCuadro(){

        System.out.println("\n\nMOSTRANDO TABLA DE CITAS PREVIAS");
        System.out.println("==================================");
        System.out.println("  HORA\t   PACIENTE");

        for (int i = 0; i < cCitas.lineaCuadro.size(); i++) {
            System.out.println(cCitas.lineaCuadro.get(i));
        }

    }

}