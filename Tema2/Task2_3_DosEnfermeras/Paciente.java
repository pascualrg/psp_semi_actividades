package psp_semi_actividades.Tema2.Task2_3_DosEnfermeras;

public class Paciente implements Runnable {

    private Enfermera enfermera;
    int numPacientes = 0;

    public Paciente(Enfermera enfermera) {
        this.enfermera = enfermera;
    }

    public void run() {

        System.out.println("[" + Thread.currentThread().getName() + "]" + " Entrando al ambulatorio.");

        //Número aleatorio que decidirá cuánto tarda en entrar el paciente al ambulatorio.
        int tiempoEspera = (int) (Math.random() * (1000 - 0 + 1) + 0);

        try {
            Thread.sleep(tiempoEspera);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int nombreEnfermera = (int) (Math.random() * (1 - 0 + 1) + 0);

        if(nombreEnfermera==0){
            enfermera.setNombreEnfermera("Enfermera1");
        }else{
            enfermera.setNombreEnfermera("Enfermera2");
        }

        System.out.println("[" + Thread.currentThread().getName() + "]" + " Solicitando Cita Previa a la "+enfermera.getNombreEnfermera()+" ..");
        
        System.out.println("[" + Thread.currentThread().getName() + "] " + enfermera.getNombreEnfermera()+" busca un espacio en el cuadro de citas para el paciente "+Thread.currentThread().getName());

        enfermera.asignarCita(Thread.currentThread().getName());
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("[" + Thread.currentThread().getName() + "]" + " Saliendo del ambulatorio.");

    }

}
