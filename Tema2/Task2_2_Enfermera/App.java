package psp_semi_actividades.Tema2.Task2_2_Enfermera;


public class App {
    public static void main(String[] args) throws InterruptedException {
        
        System.out.println("["+Thread.currentThread().getName()+"] Inicia el cuadro de citas");
        System.out.println("["+Thread.currentThread().getName()+"] La enfermera coge el cuadro de citas");

        //El valor que se pasa cuando se crea la enfermera es el maximo de citas disponibles.
        //El máximo permitido es 8, ya que no se han puesto más turnos. de 10:00h a 12:00h (cada 15 min)
        Enfermera enfermera = new Enfermera(8);

        //Número de pacientes que entraran en el ambulatorio.
        Thread[] pacientes = new Thread[10];

        for (int i = 0; i <pacientes.length ; i++) {
            Paciente paciente = new Paciente(enfermera);
            pacientes[i] = new Thread(paciente, "Paciente "+(i+1));
            pacientes[i].start();
        }

        for (Thread thread : pacientes) {
            thread.join();
        }
        
        enfermera.mostrarCuadro();
    }

    public Enfermera enviarEnfermera(Enfermera enf){
        return enf;
    }
}
